function initModal(){

    var navbar = document.getElementById('navbar');
    var modal = document.getElementById('myModal');

    var imgs = document.getElementsByClassName('imgToModal');
    var modalImg = document.getElementById("imgFModal");
    var captionText = document.getElementById("captionFModal");
    var i;
    for (i = 0; i < imgs.length; i++) {
        var img = imgs[i];
        img.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
            navbar.style.display = "none";
        }
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("imgModalClose")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() { 
        modal.style.display = "none";
        navbar.style.display = "flex";
    }
}